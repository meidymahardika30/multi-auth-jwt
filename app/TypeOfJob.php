<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TypeOfJob extends Model
{
    protected $fillable = [
    	'id',
    	'name'
    ];

    public function data_applicant(){
        return $this->belongsTo('App\DataApplicant');
    }
}
