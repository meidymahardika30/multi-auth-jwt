<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class Companies
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!auth('companies')->check() && !auth('companies')->user()){
            return response()->json(['error' => 'Unauthorized'], 401);
        }

        return $next($request);
    }
}
