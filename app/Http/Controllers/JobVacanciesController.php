<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Companies;
use App\JobVacancies;
use Validator;

class JobVacanciesController extends Controller
{
    public function index(){
        $job_vacancies = JobVacancies::all();
        
        return response()->json(['success'=>$job_vacancies], 200);
    }

    public function store(Request $request){
    	$validator = Validator::make($request->all(), [ 
            'name_of_job' => 'required', 
            'type_of_job_id' => 'required', 
            'address' => 'required',
        ]);

        if ($validator->fails()) { 
            return response()->json(['error' => 'Unauthorized'], 401);            
        }

        $job_vacancies = new JobVacancies();
        $job_vacancies->companies_id = auth('companies')->user()->id;
        $job_vacancies->name_of_job = $request->get('name_of_job');
        $job_vacancies->type_of_job_id = $request->get('type_of_job_id');
        $job_vacancies->address = $request->get('address');
        $job_vacancies->qty_of_applicants = 0;
        $job_vacancies->status = 0;

        if($job_vacancies->save()){
        	return response()->json(['success'=>$job_vacancies], 200);
        }else{
        	return response()->json(['error' => 'Unauthorized'], 401);
        }
    }

    public function update($id, Request $request)
    {
        $job_vacancies = JobVacancies::find($id);

        if (!$job_vacancies){
            return response()->json(['error' => 'Unauthorized'], 401); 
        }

        $job_vacancies->name_of_job = $request->get('name_of_job');
        $job_vacancies->type_of_job_id = $request->get('type_of_job_id');
        $job_vacancies->address = $request->get('address');
        $job_vacancies->status = $request->get('status');

        if(!$job_vacancies->save()){
            return response()->json(['error' => 'Unauthorized'], 401);
        }

        return response()->json(['success'=>$job_vacancies], 200);
    }
}
