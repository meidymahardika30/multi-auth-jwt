<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Jobseeker;  
use Validator;

class JobseekerController extends Controller
{
	public function __construct()
    {
        // $this->middleware('auth:jobseekers', ['except' => ['login', 'register']]);
    }

    public function register(Request $request) 
    { 
        $validator = Validator::make($request->all(), [ 
            'name' => 'required', 
            'email' => 'required|email', 
            'password' => 'required', 
            'c_password' => 'required|same:password', 
        ]);

		if ($validator->fails()) { 
            return response()->json(['error' => 'Unauthorized'], 401);            
        }

		$input = $request->all(); 
		        $input['password'] = bcrypt($input['password']); 
		        $jobseeker = Jobseeker::create($input); 

		return response()->json(['success'=>$jobseeker], 200); 
	}

    public function login()
    {
        $credentials = request(['email', 'password']);

        if (! $token = auth('jobseekers')->attempt($credentials)) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }

        return $this->respondWithToken($token);
    }

    public function me(Request $request)
    {   
        return response()->json(auth('jobseekers')->user());
    }

    public function logout()
    {
        auth()->logout();

        return response()->json(['message' => 'Successfully logged out']);
    }

    public function refresh()
    {
        return $this->respondWithToken(auth('jobseekers')->refresh());
    }

    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth('jobseekers')->factory()->getTTL() * 60
        ]);
    }

    public function payload()
    {
        return auth('jobseekers')->payload();
    }
}
