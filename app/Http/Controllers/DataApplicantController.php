<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Jobseeker;
use App\JobVacancies;
use App\DataApplicant;
use Validator;

class DataApplicantController extends Controller
{
	public function index()
    {
		$jobseeker_id = auth('jobseekers')->user()->id;
        $data_applicant = DataApplicant::where('jobseeker_id',$jobseeker_id)->get();
        
        return response()->json(['success'=>$data_applicant], 200);
    }

    public function store(Request $request)
    {
    	$validator = Validator::make($request->all(), [
            'job_vacancies_id' => 'required',
            'address' => 'required', 
        ]);

        if ($validator->fails()) { 
            return response()->json(['error' => 'Unauthorized'], 401);         
        }

        if(JobVacancies::where('status',1)->get()){
            return response()->json(['error' => 'Unauthorized'], 401);
        }

        $data_applicant = new DataApplicant();
        $data_applicant->jobseeker_id = auth('jobseekers')->user()->id;
        $data_applicant->job_vacancies_id = $request->get('job_vacancies_id');
        $data_applicant->address = $request->get('address');

        $jobseeker_id = auth('jobseekers')->user()->id;
        $jobseeker = DataApplicant::where('jobseeker_id',$jobseeker_id)->select('jobseeker_id')->value('jobseeker_id');
        $job_vacancies_id = $request->get('job_vacancies_id');
        $job_vacancies = JobVacancies::where('id',$job_vacancies_id)->select('id')->value('id');
        
        if($jobseeker && $job_vacancies){
            return response()->json(['error' => 'Unauthorized'], 401);
        }

        $job_vacancies = JobVacancies::find($request->get('job_vacancies_id'));
        $qty_of_applicants = JobVacancies::where('id',$request->get('job_vacancies_id'))->select('qty_of_applicants')->value('qty_of_applicants');
        $job_vacancies->qty_of_applicants = $qty_of_applicants+1;

        if($data_applicant->save() && $job_vacancies->save()){
            return response()->json(['success'=>$data_applicant], 200);
        }else{
            return response()->json(['error' => 'Unauthorized'], 401);
        }
    }

    public function update($id, Request $request)
    {
        $data_applicant = DataApplicant::find($id);

        if (!$data_applicant){
            return response()->json(['error' => 'Unauthorized'], 401); 
        }

        $data_applicant->job_vacancies_id = $request->get('job_vacancies_id');
        $data_applicant->address = $request->get('address');

        if(!$data_applicant->save()){
            return response()->json(['error' => 'Unauthorized'], 401);
        }

        return response()->json(['success'=>$data_applicant], 200);
    }
}
