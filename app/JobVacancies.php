<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JobVacancies extends Model
{
    protected $fillable = [
    	'id',
        'name_of_job',
        'type_of_job_id', 
        'address',
        'qty_of_applicant',
        'status'
    ];

    public function job_vacancies(){
        return $this->belongsTo('App\JobVacancies');
    }
}
