<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DataApplicant extends Model
{
    protected $fillable = [
    	'id',
        'jobseeker_id',
        'job_vacancies_id', 
        'address'
    ];

    public function data_applicant(){
        return $this->belongsTo('App\DataApplicant');
    }
}
