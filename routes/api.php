<?php

Route::post('users/login', 'UserController@login');
Route::post('users/logout', 'UserController@logout');
Route::post('users/register', 'UserController@register');

Route::group(['middleware' => 'users','prefix' => 'users'], function () {
    
    Route::post('refresh', 'UserController@refresh');
    Route::post('me', 'UserController@me');
    Route::post('payload', 'UserController@payload');

});

Route::post('jobseekers/login', 'JobseekerController@login');
Route::post('jobseekers/logout', 'JobseekerController@logout');
Route::post('jobseekers/register', 'JobseekerController@register');

Route::group(['middleware' => 'jobseekers','prefix' => 'jobseekers'], function () {

    Route::post('refresh', 'JobseekerController@refresh');
    Route::post('me', 'JobseekerController@me');
    Route::post('payload', 'JobseekerController@payload');

    Route::group(['prefix' => 'data_applicants'], function () {

        Route::get('list', 'DataApplicantController@index');
        Route::post('create', 'DataApplicantController@store');
        Route::post('update/{id}', 'DataApplicantController@update');
        
    });
});

Route::post('companies/login', 'CompaniesController@login');
Route::post('companies/logout', 'CompaniesController@logout');
Route::post('companies/register', 'CompaniesController@register');

Route::group(['middleware' => 'companies','prefix' => 'companies'], function () {

    Route::post('refresh', 'CompaniesController@refresh');
    Route::post('me', 'CompaniesController@me');
    Route::post('payload', 'CompaniesController@payload');

    Route::group(['prefix' => 'job_vacancies'], function () {

        Route::get('list', 'JobVacanciesController@index');
	    Route::post('create', 'JobVacanciesController@store');
        Route::post('update/{id}', 'JobVacanciesController@update');

	});
});
