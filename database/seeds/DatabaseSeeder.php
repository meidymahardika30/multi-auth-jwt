<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Jobseeker;
use App\Companies;
use App\TypeOfJob;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);

        User::create([
            'name'=>'qwe',
            'email' => 'qwe@gmail.com',
            'password' => Hash::make('123456'),
        ]);

        Jobseeker::create([
            'name'=>'asd',
            'email' => 'asd@gmail.com',
            'password' => Hash::make('123456'),
        ]);

        Jobseeker::create([
            'name'=>'ert',
            'email' => 'ert@gmail.com',
            'password' => Hash::make('123456'),
        ]);

        Jobseeker::create([
            'name'=>'rty',
            'email' => 'qwe@gmail.com',
            'password' => Hash::make('123456'),
        ]);

        Companies::create([
            'name'=>'zxc',
            'email' => 'zxc@gmail.com',
            'password' => Hash::make('123456'),
        ]);

        Companies::create([
            'name'=>'edc',
            'email' => 'edc@gmail.com',
            'password' => Hash::make('123456'),
        ]);

        Companies::create([
            'name'=>'fgh',
            'email' => 'qwe@gmail.com',
            'password' => Hash::make('123456'),
        ]);

        TypeOfJob::create([
            'name'=>'Programmer'
        ]);

        TypeOfJob::create([
            'name'=>'Marketing'
        ]);

        TypeOfJob::create([
            'name'=>'Designer'
        ]);
    }
}
